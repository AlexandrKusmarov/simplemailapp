package mailapp.controller;

import mailapp.domain.User;
import mailapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class UserController {

    private final static Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/user/profile")
    public String edit(Model model,
                       @AuthenticationPrincipal User currentUser){

        model.addAttribute("currentUser", currentUser);
        logger.info("Open profile of user: " + currentUser.getUsername());

        return "userProfile";
    }

    @PostMapping("/user/profile")
    public String userSave(
            @AuthenticationPrincipal User user,
            @RequestParam String username,
            @RequestParam String surname,
            @RequestParam String password,
            @RequestParam @Valid String phone,
            @RequestParam @Valid String email_register
    ) {

        user.setUsername(username);
        logger.info("Changed username to: " + username);
        user.setSurname(surname);
        logger.info("Changed surname to: " + surname);
        user.setPassword(passwordEncoder.encode(password));
        logger.info("Changed password to: " + password);
        user.setPhone(phone);
        logger.info("Changed phone to: " + phone);
        user.setEmail_register(email_register);
        logger.info("Set email into table emailaddr: " + email_register);
        userService.save(user);
        logger.info("Save changes, redirect:/");
        return "redirect:/";
    }


}
