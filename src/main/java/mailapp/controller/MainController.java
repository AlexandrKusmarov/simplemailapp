package mailapp.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class MainController {

    private final static Logger logger = Logger.getLogger(MainController.class);

    @GetMapping("/")
    public String hello(Map<String, Object> model) {

        logger.info("Open main page.");

        return "main";
    }

    @GetMapping("/personalcabinet")
    public String personalCabinet(Map<String, Object> model){

        logger.info("Open personalcabinet page.");

        return "personalcabinet";
    }

}
