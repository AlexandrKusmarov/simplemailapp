package mailapp.controller;

import mailapp.domain.EmailAddr;
import mailapp.domain.User;
import mailapp.service.EmailService;
import mailapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.Date;
import java.util.Map;

@Controller
public class RegistrationController {

    private final static Logger logger = Logger.getLogger(RegistrationController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @GetMapping("/registration")
    public String registration(){
        logger.info("Open registration form.");
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @Valid User user,
            BindingResult bindingResult,
            Model model)
    {

        if(user.getPassword() == null){
            model.addAttribute("passwordError", "Passwords can't be empty");
        }


        if(bindingResult.hasErrors()){
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errors);

            return "registration";
        }

        if(!userService.addUser(user)){
            model.addAttribute("usernameError", "User exists!");
            logger.error("User exists!");

            return "registration";
        }

        EmailAddr emailAddr = new EmailAddr(user.getEmail_register(),new Date(), user.getUsername());
        emailAddr.setUser(user);
        emailService.save(emailAddr);
        logger.info("User " + user.getUsername() + " was created!");
        logger.info("redirect:/login");

        return "redirect:/login";
    }
}
