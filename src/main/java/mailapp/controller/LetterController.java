package mailapp.controller;

import mailapp.domain.Letter;
import mailapp.domain.User;
import mailapp.repository.LetterRepository;
import mailapp.service.LetterService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;
import java.util.Date;

@Controller
public class LetterController {

    private static final Logger logger = Logger.getLogger(LetterController.class);

    @Autowired
    private LetterRepository letterRepository;

    @Autowired
    private LetterService letterService;

    @GetMapping("/letter")
    public String letterListOut(Model model){
        model.addAttribute("letters", letterRepository.findAll());
        model.addAttribute("size", letterRepository.findAll().size());
        logger.info("Open page with OUT letters");

        return "letter";
    }

    @GetMapping("/letterIn")
    public String letterListIn(Model model){
        model.addAttribute("letters", letterRepository.findAll());
        logger.info("Open page with IN letters");

        return "letterIn";
    }

    @GetMapping("/letter/delete/{id_letter}")
    public String letterEditForm(@PathVariable ("id_letter") long id_letter){
        letterService.deleteLetter(id_letter);
        logger.info("Delete letter with id: " + id_letter);
        logger.info("redirect:/letter");

        return "redirect:/letter";
    }

    @GetMapping("/writeLetter")
    public String view(Model model){
        logger.info("Open page of writing letters.");

        return "writeLetter";
    }

    @PostMapping("/writeLetter")
    public String save(@AuthenticationPrincipal User user,
                       @RequestParam String to_user,
                       @RequestParam String text,
                       @RequestParam String theme,
                       @RequestParam File ffile
    ) {
        Date date = new Date();
        Letter letter = new Letter(null, to_user, date, theme, text, ffile);
        letterService.save(letter);
        letter = letterService.findByIdletter(letter.getIdLetter());
        letter.setAuthor(user);
        logger.info("Set author of the writing letter: " + user);
        letter.setFromUser(user.getEmail_register());
        letter.setFfile(ffile);
        letterService.save(letter);
        logger.info("Save letter. redirect:/");

        return "redirect:/";
    }


}
