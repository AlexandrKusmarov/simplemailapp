package mailapp.controller;

import mailapp.domain.Folder;
import mailapp.domain.User;
import mailapp.service.FolderService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FolderController {

    private static final Logger logger = Logger.getLogger(FolderController.class);

    @Autowired
    private FolderService folderService;



    @GetMapping("/letter/folderlist")
    public String folderList(Model model){
        model.addAttribute("folders", folderService.findAllFolders());
        logger.info("Open folderlist page.");

        return "folderlist";
    }

    @GetMapping("/letter/folderlist/newfolder")
    public String createFolder(Model model){
        model.addAttribute("folders", folderService.findAllFolders());
        logger.info("Open newfolder page.");

        return "newfolder";
    }

    @PostMapping("/letter/folderlist/newfolder")
    public String saveFolder(@AuthenticationPrincipal User user,
                       @RequestParam String foldername
    ) {
        Folder folder = new Folder(foldername);
        logger.info("Create folder : " + foldername);
        folderService.save(folder);
        folder = folderService.findById(folder.getIdfolder());
        folder.setAuthor(user);
        logger.info("Set author of folder : " + user);
        folderService.save(folder);
        logger.info("redirect:/letter/folderlist");

        return "redirect:/letter/folderlist";
    }

    @GetMapping("/letter/folderlist/delete/{idfolder}")
    public String deleteFolder(@PathVariable("idfolder") long idfolder){
        folderService.deleteFolder(idfolder);
        logger.info("Delete folder by ID : " + idfolder);
        logger.info("redirect:/letter/folderlist");

        return "redirect:/letter/folderlist";
    }

    @GetMapping("/letter/folderedit/{idfolder}")
    public String getFolder(Model model,
                            @PathVariable Long idfolder){

        model.addAttribute("folders", folderService.findById(idfolder));
        logger.info("Open edit folder page by id: " + idfolder);

        return "foldereditor";
    }

    @PostMapping("/letter/folderedit/{idfolder}")
    public String folderSave(
            @AuthenticationPrincipal User user,
            @RequestParam String foldername,
            @PathVariable Long idfolder
    ) {

        Folder folder = folderService.findById(idfolder);
        folder.setFoldername(foldername);
        logger.info("Change folder name to :" + foldername);
        folderService.save(folder);
        logger.info("Save folder. redirect:/letter/folderlist");

        return "redirect:/letter/folderlist";
    }


}
