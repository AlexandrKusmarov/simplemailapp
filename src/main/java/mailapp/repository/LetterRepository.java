package mailapp.repository;

import mailapp.domain.Letter;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.Set;

public interface LetterRepository extends CrudRepository<Letter, Long> {
    Set<Letter> findAll();

    @Override
    void deleteById(Long aLong);

    Letter findByTheme(String theme);

    Letter findByIdLetter (Long id);



}
