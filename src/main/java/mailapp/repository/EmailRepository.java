package mailapp.repository;

import mailapp.domain.EmailAddr;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailRepository extends CrudRepository<EmailAddr, Long> {

}
