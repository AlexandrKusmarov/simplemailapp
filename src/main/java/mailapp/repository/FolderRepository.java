package mailapp.repository;

import mailapp.domain.Folder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface FolderRepository extends CrudRepository<Folder, Long> {

    Set<Folder> findAll();

    Folder findByIdfolder(Long aLong);

    void deleteById(Long aLong);
}
