package mailapp.service;

import mailapp.domain.Letter;
import mailapp.repository.LetterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LetterService {

    @Autowired
    private LetterRepository letterRepository;

    public void deleteLetter(long id){
        letterRepository.deleteById(id);
    }

    public void save(Letter letter){
        letterRepository.save(letter);
    }

    public Letter findByIdletter(Long id){
        return letterRepository.findByIdLetter(id);
    }
}
