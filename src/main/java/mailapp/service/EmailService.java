package mailapp.service;

import mailapp.domain.EmailAddr;
import mailapp.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    private EmailRepository emailRepository;

    public void save(EmailAddr emailAddr){
        emailRepository.save(emailAddr);
    }
}
