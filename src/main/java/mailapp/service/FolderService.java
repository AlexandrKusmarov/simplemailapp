package mailapp.service;

import mailapp.domain.Folder;
import mailapp.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class FolderService {

    @Autowired
    private FolderRepository folderRepository;

    public Set<Folder> findAllFolders(){
        return folderRepository.findAll();
    }

    public void save(Folder folder){
        folderRepository.save(folder);
    }

    public Folder findById(Long id){
        return folderRepository.findByIdfolder(id);
    }

    public void deleteFolder(Long id){
        folderRepository.deleteById(id);
    }




}
