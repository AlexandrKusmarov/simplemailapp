package mailapp.domain;

import javax.persistence.*;
import java.io.File;
import java.util.Date;
import java.util.Objects;

@Entity
public class Letter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idLetter;

    private String fromUser;

    private String toUser;

    private Date date;

    private String theme;

    private String text;

    private File ffile;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id")
    private User author;

//    @OneToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "letter")
//    private Folder folder;

    public Letter() {
    }

    public Letter(String fromUser, String toUser, Date date, String theme, String text, File ffile) {
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.date = date;
        this.theme = theme;
        this.text = text;
        this.ffile = ffile;
    }

//        public Folder getFolder() {
//        return folder;
//    }
//
//    public void setFolder(Folder folder) {
//        this.folder = folder;
//    }


    public File getFfile() {
        return ffile;
    }

    public void setFfile(File file) {
        this.ffile = file;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Long getIdLetter() {
        return idLetter;
    }

    public void setIdLetter(Long idLetter) {
        this.idLetter = idLetter;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
