package mailapp.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Folder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idfolder;

    @JoinColumn(name = "foldername")
    private String foldername;

    private Integer letter;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id")
    private User authorfolder;

//    @OneToMany(fetch = FetchType.EAGER,mappedBy = "folder")
//    private Set<Letter> letterSet;

    public Folder() {
    }

    public Folder(String foldername) {
        this.foldername = foldername;
    }

//    public Set<Letter> getLetterSet() {
//        return letterSet;
//    }
//
//    public void setLetterSet(Set<Letter> letterSet) {
//        this.letterSet = letterSet;
//    }

    public User getAuthor() {
        return authorfolder;
    }

    public void setAuthor(User author) {
        this.authorfolder = author;
    }

    public Long getIdfolder() {
        return idfolder;
    }

    public void setIdfolder(Long idfolder) {
        this.idfolder = idfolder;
    }

    public String getFoldername() {
        return foldername;
    }

    public void setFoldername(String foldername) {
        this.foldername = foldername;
    }

    public Integer getLetter() {
        return letter;
    }

    public void setLetter(Integer letter) {
        this.letter = letter;
    }
}
