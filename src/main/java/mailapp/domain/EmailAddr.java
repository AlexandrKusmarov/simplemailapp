package mailapp.domain;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "emailaddr")
public class EmailAddr {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_EmailAddress;

    @Email(message = "Email is not correct.")
    @NotBlank(message = "Password cannot be empty.")
    private String email;

    private Date dateOfCreate;

    @NotBlank(message = "Owner cannot be empty.")
    private String owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id")
    private User user;

    public EmailAddr() {
    }

    public EmailAddr(String email, Date dateOfCreate, String owner) {
        this.email = email;
        this.dateOfCreate = dateOfCreate;
        this.owner = owner;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public Long getId_EmailAddress() {
        return id_EmailAddress;
    }

    public void setId_EmailAddress(Long id_EmailAddress) {
        this.id_EmailAddress = id_EmailAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfCreate() {
        return dateOfCreate;
    }

    public void setDateOfCreate(Date dateOfCreate) {
        this.dateOfCreate = dateOfCreate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}

