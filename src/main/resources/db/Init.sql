CREATE database if not exists mails;

SET NAMES 'utf8';

CREATE table if not exists Usr(
  id_usr integer(10) AUTO_INCREMENT,
  username varchar(255) not null,
  surname varchar(255),
  password varchar(255) not null,
  phone varchar(20),
  active boolean not null,
  email_register varchar(100) not null,
  PRIMARY KEY (id_usr)
);

CREATE table if not exists Letter(
  idLetter integer(10) AUTO_INCREMENT,
  fromUser varchar(255) not null,
  toUser varchar(255) not null,
  date datetime not null,
  theme varchar(255),
  text varchar(255),
  ffile longblob,
  usr_id integer not null,
  fold_id integer not null,
  PRIMARY KEY (idLetter)
);

CREATE table if not exists EmailAddr(
  id_EmailAddress integer(10) AUTO_INCREMENT,
  email varchar(50) not null,
  dateOfCreate datetime not null,
  owner varchar(255) not null,
  usr_id integer(10) not null,
  foldr_id integer(10) not null,
  PRIMARY KEY (id_EmailAddress)
);

CREATE table if not exists Folder(
  idfolder int(10) AUTO_INCREMENT,
  folderName varchar(100) not null,
  emailAddr integer(10),
  letter integer(10),
  usr_id integer(10),
  PRIMARY KEY (idfolder)
);


create table user_role (
  user_id integer(10) not null,
  roles varchar(255)
);

ALTER TABLE usr AUTO_INCREMENT = 10;

alter table Letter
  add constraint letter_user_fk
FOREIGN KEY (usr_id) REFERENCES Usr (id_usr) on DELETE cascade;

alter table Letter
  add constraint letter_folder_fk
FOREIGN KEY (fold_id) REFERENCES Folder (idfolder) on DELETE cascade;

alter table EmailAddr
  add constraint EmailAddr_folder_fk
foreign key (foldr_id) references Folder(idfolder) on delete cascade;

alter table EmailAddr
  add constraint EmailAddr_user_fk
FOREIGN KEY (usr_id) REFERENCES Usr(id_usr) on delete CASCADE;

alter table folder
  add constraint folder_user_fk
FOREIGN KEY (usr_id) REFERENCES usr(id_usr) on delete cascade;

alter table user_role
  add constraint user_role_user_fk
foreign key (user_id) references usr(id_usr);

SET FOREIGN_KEY_CHECKS =0;

insert into Folder (idfolder, folderName, emailAddr, letter, usr_id)
VALUES(5,'papka', 4, 8, 3 );

SET FOREIGN_KEY_CHECKS =1;
