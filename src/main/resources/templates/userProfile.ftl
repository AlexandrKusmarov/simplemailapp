<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page>

<div class="row justify-content-center">
    <h1>Edit your profile</h1>
</div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit profile</div>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="/user/profile">
                            <div class="form-group">
                                <label for="username" class="cols-sm-2 control-label">Username</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="username" id="username" value="${currentUser.getUsername()}" placeholder="Enter username" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email_register" class="cols-sm-2 control-label">Your Email</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="email_register" id="email_register" value="${currentUser.getEmail_register()}" placeholder="Enter your Email" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label">Password</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="password" id="password" value="${currentUser.getPassword()}" placeholder="Enter your Password" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="surname" class="cols-sm-2 control-label">Your Surname</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="surname" id="surname" <#if currentUser.getSurname()??> value="${currentUser.getSurname()}" <#else> value="" </#if> placeholder="Enter your Surname" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="cols-sm-2 control-label">Your Phone</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="phone" id="phone" <#if currentUser.getPhone()??> value="${currentUser.getPhone()}" <#else> value="" </#if> placeholder="Enter your Phone number" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Apply changes</button>
                                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

</@c.page>