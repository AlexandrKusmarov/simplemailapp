<#import "parts/common.ftl" as p>
<#include "parts/security.ftl">

<@p.page>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create your letter</div>
                <div class="card-body">

                    <form class="form-horizontal" action="/writeLetter" method="post">

                        <div class="form-group">
                            <label for="to_user" class="cols-sm-2 control-label">Email:</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa"
                                                                       aria-hidden="true"></i></span>
                                    <input class="form-control" type="text" name="to_user" id="to_user" placeholder="to">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text" class="cols-sm-2 control-label">Text:</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input class="form-control" type="text" name="text" id="text" placeholder="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="theme" class="cols-sm-2 control-label">Theme:</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input class="form-control" type="text" name="theme" id="theme" placeholder="theme">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ffile" class="cols-sm-2 control-label">File:</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input class="form-control" type="file" name="ffile" id="ffile" placeholder="theme">
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Send</button>
                            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</@p.page>