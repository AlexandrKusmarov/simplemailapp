<#import "parts/common.ftl" as p>
<#include "parts/security.ftl">

<@p.page>

  <table class="table table-bordered">
      <thead>
      <tr>
          <th>From user</th>
          <th>To user</th>
          <th>Date</th>
          <th>Theme</th>
          <th>Text</th>
          <th>File</th>
      </tr>
      </thead>
      <tbody>
    <#list letters as letter>
    <#if currentUserId == letter.getAuthor().getId_usr()>
    <tr>
        <td><#if letter.fromUser??> ${letter.fromUser}</#if></td>
        <td><#if letter.toUser??> ${letter.toUser}</#if> </td>
        <td>${letter.date}</td>
        <td>${letter.theme}</td>
        <td>${letter.text}</td>
        <td><#if letter.file??> ${letter.getFile()}</#if> </td>
    <td><a href="/letter/delete/${letter.idLetter}">Delete</a></td>
        <td><a href="/letter/FoldName/">FoldName</a></td>
    </tr>
    </#if>
    </#list>
      </tbody>
  </table>
</@p.page>