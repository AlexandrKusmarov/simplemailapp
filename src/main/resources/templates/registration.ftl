<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Register</div>
                <div class="card-body">

                    <form class="form-horizontal" method="post" action="/registration">

                        <div class="form-group">
                            <label for="username" class="cols-sm-2 control-label">Username</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control ${(usernameError??)?string('is-invalid', '')}" name="username" id="username" placeholder="Enter username" />
                                        <#if usernameError??>
                                            <div class="invalid-feedback">
                                                ${usernameError}
                                            </div>
                                        </#if>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email_register" class="cols-sm-2 control-label">Your Email</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control ${(emailError??)?string('is-invalid', '')}" name="email_register" id="email_register" placeholder="Enter your Email" />
                                        <#if emailError??>
                                            <div class="invalid-feedback">
                                                ${emailError}
                                            </div>
                                        </#if>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="cols-sm-2 control-label">Password</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control ${(passwordError??)?string('is-invalid', '')}" name="password" id="password" placeholder="Enter your Password" />
                                        <#if passwordError??>
                                            <div class="invalid-feedback">
                                                ${passwordError}
                                            </div>
                                        </#if>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">

                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                            <input type="hidden" name="_csrf" value="${_csrf.token}" />
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</@c.page>