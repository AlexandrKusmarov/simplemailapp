<#import "parts/common.ftl" as p>
<#include "parts/security.ftl">

<@p.page>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create your folder</div>
                <div class="card-body">

                    <form class="form-horizontal" action="/letter/folderlist/newfolder" method="post">

                        <div class="form-group">
                            <label for="foldername" class="cols-sm-2 control-label">Folder name</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa"
                                                                       aria-hidden="true"></i></span>
                                    <input class="form-control" type="text" name="foldername" id="foldername" placeholder="folder name">
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Create</button>
                            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</@p.page>