<#import "parts/common.ftl" as p>
<#include "parts/security.ftl">

<@p.page>

<div>
    <a href="/letter/folderlist/newfolder">Create new folder</a>
</div>

<div class="row justify-content-center">
    <h1>Folders List</h1>
</div>
<table class="table table-bordered">

    <tbody>
    <#list folders as currentfolder>
    <#if currentUserId == currentfolder.getAuthor().getId_usr()>
    <tr>
        <td>${currentfolder.foldername}</td>
        <td>
            <a href="/letter/folderlist/delete/${currentfolder.idfolder}">Delete</a>
            <a href="/letter/folderedit/${currentfolder.idfolder}">Edit</a>
        </td>
    </tr>
    </#if>
    </#list>
    </tbody>
</table>

</@p.page>