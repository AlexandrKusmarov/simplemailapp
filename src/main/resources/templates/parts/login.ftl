<#macro login path>

<div class="container login-container">
    <div class="row">
        <div class="col text-center login-form-1">
            <h3>Login Form </h3>
            <form action="/login" method="post">
                <div class="form-group">
                    <input type="text" class="form-control ${(usernameError??)?string('is-invalid', '')}" placeholder="Username" name="username" />
                        <#if usernameError??>
                            <div class="invalid-feedback">
                                ${usernameError}
                            </div>
                        </#if>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control ${(passwordError??)?string('is-invalid', '')}" placeholder="Password" name="password" />
                        <#if passwordError??>
                            <div class="invalid-feedback">
                                ${passwordError}
                            </div>
                        </#if>
                </div>
                <div class="form-group">
                    <input type="hidden" name="_csrf" value="${_csrf.token}" />
                    <input type="submit" class="btnSubmit" value="Login" />
                </div>
            </form>
        </div>

    </div>
</div>
</#macro>

<#macro logout>
<form action="/logout" method="post">
    <input type="hidden" name="_csrf" value="${_csrf.token}" />
    <button class="btn btn-primary" type="submit"><#if user??>Log In<#else>Log Out</#if></button>
</form>
</#macro>


