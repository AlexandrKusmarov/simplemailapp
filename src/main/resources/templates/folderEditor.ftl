<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page>

<div class="row justify-content-center">
    <h1>Edit your folder</h1>
</div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit folder</div>
                    <div class="card-body">
                        <form class="form-horizontal2" method="post" action="/letter/folderedit/${folders.getIdfolder()}">
                            <div class="form-group">
                                <label for="foldername" class="cols-sm-2 control-label">Foldername</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="foldername" id="foldername" value="${folders.foldername}" placeholder="Enter username" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Apply changes</button>
                                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

</@c.page>