Инструкция по деплою приложения на Amazon.com
                    Подготовка сервера, запуск.
    1. Создаем аккаунт на Amazon.com.
    2. Создаем новый веб-проект (EC2) на базе Amazon Linux 2 Ami.
    3. Создаем правила безопасности нашей группы (security group).
        3.1  Включаем доступ к серверу по SSH (порт 22. для владельца сервера).
        3.2  Включаем доступ к серверу по HTTP протоколу для всех.
    4. Генерируем SSH ключ. Файл с ключем сохраняем на локальный компьютер.
    5. Сервер запущен, состояние ок.
    
                    Настройка сервера, установка необходимого по.
    6. Подключаемся по SSH ключу через терминал в вашей IDE к нашему удаленному серверу.
    Пример: ssh -i ~/.ssh/emailsimulator.pem ec2-user@ec2-52-56-186-49.eu-west-2.compute.amazonaws.com
        ssh -i ~/.ssh/emailsimulator.pem - SSH файл, который был сгенерирован в П. 4.
        ec2-user - имя удаленной машина амазон по умолчанию.
        ec2-52-56-186-49.eu-west-2.compute.amazonaws.com - адресс нашего сервера(public domain) Можно найти в личном кабинете амазона. .
    7. Если все правильно указали без ошибок и т. д., попадаем в терминал нашего удаленного сервера.
    8. Следующим шагом будет установка базы данных. В нашем случае необходимо было использовать MySql.
                                    Установка MySql.
        1. Обновим систему до последней версии: sudo yum update
        2. Скачиваем дистрибутив:  wget http://repo.mysql.com/mysql57-community-release-el7.rpm
        3. Установим пакет: sudo rpm -ivh mysql57-community-release-el7.rpm
        4. Снова обновляемся: sudo yum update
        5. Устанавливаем сервер: sudo yum install mysql-server
        6. Запускаем службу mySql: sudo systemctl start mysqld
                                    Настройка MySql.
        1. Запускаем скрипт для настройки бд: mysql_secure_installation
            С помощью скрипта мы можем установить пароль для пользователя по умолчанию root.
        2. Если система не предложила задать пароль, выполните команду: grep 'temporary password' /var/log/mysqld.log
        это позволит вам посмотреть пароль по умолчанию (генерируется системой). В дальнейшем его можно поменять.
        3. Далее создадим бд mysql(если вы используете бд не по умолчанию).
        4. Выполните: sudo mysql -u root -p
        5. Далее: create database name_database;
        
                                    Установка Java
        1. sudo yum install java-1.8.0-openjdk-devel
        
                                    Установка Nginx
        1. sudo amazon-linux-extras install nginx1.12
        2. Редактируем конфигурацию: sudo vi  /etc/nginx/nginx.conf
        3. Меняем имя пользователя. Было "nginx" стало "ec2-user"
        4. обновляем раздел location так:
           
           location / {
               proxy_set_header Host $host;
               proxy_set_header X-Real-IP $remote_addr;
               proxy_pass http://localhost:8080;
           }
        
        5. Открываем доступ пользователю ec2-user к папке сервера nginx:
            sudo chown -Rf ec2-user:ec2-user /var/lib/nginx
        6. Запускаем сервер: sudo service nginx start
            если нужен перезапуск сервера: sudo nginx -s reload
            если нужно посмотреть логи nginx: sudo vim /var/log/nginx/error.log
        7. Если не возникло ошибок, все звезды сошлись и т. д. то вам очень повезло.
                        
                          Запускаем скрипт из терминала локальной машины:
                          ./scripts/deploy.sh
                          
                          Исходный код скрипта :
                          
                                #!/usr/bin/env bash
                                
                                mvn clean install
                                
                                echo 'Copy fiels...'
                                
                                scp -i ~/.ssh/emailsimulator.pem \target/SimpleMailApp-1.0-SNAPSHOT.jar \ec2-user@ec2-52-56-186-49.eu-west-2.compute.amazonaws.com:~/
                                echo 'Restarting server...'
                                
                                ssh -i ~/.ssh/emailsimulator.pem ec2-user@ec2-52-56-186-49.eu-west-2.compute.amazonaws.com << EoF
                                pgrep java | xargs sudo kill -9
                                java -jar SimpleMailApp-1.0-SNAPSHOT.jar
                                EoF
                                
                                echo 'Bye...'
                                
        8. Наслаждаемся результатом! 
                                

    
    