#!/usr/bin/env bash

mvn clean install

echo 'Copy fiels...'

scp -i ~/.ssh/emailsimulator.pem \target/SimpleMailApp-1.0-SNAPSHOT.jar \ec2-user@ec2-52-56-186-49.eu-west-2.compute.amazonaws.com:~/
echo 'Restarting server...'

ssh -i ~/.ssh/emailsimulator.pem ec2-user@ec2-52-56-186-49.eu-west-2.compute.amazonaws.com << EoF
pgrep java | xargs sudo kill -9
java -jar SimpleMailApp-1.0-SNAPSHOT.jar
EoF

echo 'Bye...'